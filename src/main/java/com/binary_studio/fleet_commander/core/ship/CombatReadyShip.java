package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	/*
	 * OMG, I'm going to hate "this" keyword. How it is possible to read the code when the
	 * half of it is only "this" keyword ?!
	 */

	private String name;

	private PositiveInteger maxShieldHP;

	private PositiveInteger maxHullHP;

	private PositiveInteger maxCapacitor;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private int currHullHP;

	private int currShieldHp;

	private int currCapacitor;

	/*
	 * I agree that this is not the best practise for initializing an object. Maybe, It's
	 * better to make a factory here, but I don't have enough experience and can't say for
	 * sure.
	 */
	public CombatReadyShip(DockedShip dockedShip) {
		this.name = dockedShip.getName();
		this.maxShieldHP = dockedShip.getShieldHP();
		this.maxHullHP = dockedShip.getHullHP();
		this.maxCapacitor = dockedShip.getCapacitorAmount();
		this.capacitorRechargeRate = dockedShip.getCapacitorRechargeRate();
		this.speed = dockedShip.getSpeed();
		this.size = dockedShip.getSize();
		this.attackSubsystem = dockedShip.getAttackSubsystem();
		this.defenciveSubsystem = dockedShip.getDefenciveSubsystem();
		this.currCapacitor = this.maxCapacitor.value();
		this.currHullHP = this.maxHullHP.value();
		this.currShieldHp = this.maxShieldHP.value();
	}

	@Override
	public void endTurn() {

		int rechargedCapacitor = this.currCapacitor + this.capacitorRechargeRate.value();

		this.currCapacitor = rechargedCapacitor > this.maxCapacitor.value() ? this.maxCapacitor.value()
				: rechargedCapacitor;
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (!isPossibleActivateSubsystem(this.attackSubsystem)) {
			return Optional.empty();
		}
		this.currCapacitor -= this.attackSubsystem.getCapacitorConsumption().value();

		AttackAction attack = new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem);

		return Optional.of(attack);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		PositiveInteger damage = this.defenciveSubsystem.reduceDamage(attack).damage;

		if (this.currShieldHp + this.currHullHP <= damage.value()) {
			return new AttackResult.Destroyed();
		}

		if (this.currShieldHp < damage.value()) {
			this.currHullHP -= damage.value() - this.currShieldHp;
			this.currShieldHp = 0;
		}
		else {
			this.currShieldHp -= damage.value();
		}

		return new AttackResult.DamageRecived(this.attackSubsystem, damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (!isPossibleActivateSubsystem(this.defenciveSubsystem)) {
			return Optional.empty();
		}
		this.currCapacitor -= this.defenciveSubsystem.getCapacitorConsumption().value();

		PositiveInteger shieldRegeneratedPoints = getShieldRegeneratedPoints();
		PositiveInteger hullRegeneratedPoints = getHullRegeneratedPoints();

		return Optional.of(new RegenerateAction(shieldRegeneratedPoints, hullRegeneratedPoints));
	}

	private PositiveInteger getShieldRegeneratedPoints() {
		if (this.currShieldHp == this.maxShieldHP.value()) {
			return PositiveInteger.of(0);
		}

		int shieldHpAfterRegen = this.defenciveSubsystem.regenerate().shieldHPRegenerated.value() + this.currShieldHp;

		if (shieldHpAfterRegen > this.maxShieldHP.value()) {
			return PositiveInteger.of(this.maxShieldHP.value() - this.currShieldHp);
		}

		return PositiveInteger.of(shieldHpAfterRegen - this.currShieldHp);
	}

	private PositiveInteger getHullRegeneratedPoints() {
		if (this.currHullHP == this.maxHullHP.value()) {
			return PositiveInteger.of(0);
		}

		int hullHpAfterRegen = this.defenciveSubsystem.regenerate().hullHPRegenerated.value() + this.currHullHP;

		if (hullHpAfterRegen > this.maxHullHP.value()) {
			return PositiveInteger.of(this.maxHullHP.value() - this.currHullHP);
		}

		return PositiveInteger.of(hullHpAfterRegen - this.currHullHP);
	}

	private boolean isPossibleActivateSubsystem(Subsystem subsystem) {
		return this.currCapacitor >= subsystem.getCapacitorConsumption().value();
	}

	public PositiveInteger getMaxShieldHP() {
		return this.maxShieldHP;
	}

	public int getCurrShieldHp() {
		return this.currShieldHp;
	}

	public PositiveInteger getMaxHullHP() {
		return this.maxHullHP;
	}

	public int getCurrHullHP() {
		return this.currHullHP;
	}

	public PositiveInteger getMaxCapacitor() {
		return this.maxCapacitor;
	}

	public int getCurrCapacitor() {
		return this.currCapacitor;
	}

	public PositiveInteger getCapacitorRechargeRate() {
		return this.capacitorRechargeRate;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

}
