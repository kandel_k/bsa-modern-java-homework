package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger powergridRequirements;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, baseDamage, optimalSize, optimalSpeed, capacitorConsumption,
				powergridRequirments);
	}

	public AttackSubsystemImpl(String name, PositiveInteger baseDamage, PositiveInteger optimalSize,
			PositiveInteger optimalSpeed, PositiveInteger capacitorConsumption, PositiveInteger powergridRequirements) {
		this.name = name;
		this.baseDamage = baseDamage;
		this.optimalSize = optimalSize;
		this.optimalSpeed = optimalSpeed;
		this.capacitorConsumption = capacitorConsumption;
		this.powergridRequirements = powergridRequirements;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirements;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = getSizeAttackModifier(target);
		double speedReductionModifier = getSpeedAttackModifier(target);

		double damage = Math.ceil(this.baseDamage.value() * Double.min(sizeReductionModifier, speedReductionModifier));

		return PositiveInteger.of((int) damage);
	}

	private double getSizeAttackModifier(Attackable target) {
		double targetSize = target.getSize().value();

		if (targetSize >= this.optimalSize.value()) {
			return 1.0;
		}
		return targetSize / this.optimalSize.value();
	}

	private double getSpeedAttackModifier(Attackable target) {
		double targetSpeed = target.getCurrentSpeed().value();

		if (targetSpeed <= this.optimalSpeed.value()) {
			return 1.0;
		}
		return this.optimalSpeed.value() / (targetSpeed * 2);
	}

	@Override
	public String getName() {
		return this.name;
	}

	public PositiveInteger getBaseDamage() {
		return this.baseDamage;
	}

	public PositiveInteger getOptimalSize() {
		return this.optimalSize;
	}

	public PositiveInteger getOptimalSpeed() {
		return this.optimalSpeed;
	}

}
