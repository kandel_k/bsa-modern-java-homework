package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger impactReduction;

	private PositiveInteger shieldRegen;

	private PositiveInteger hullRegen;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger pgConsumption,
			PositiveInteger capacitorUsage, PositiveInteger impactReduction, PositiveInteger shieldRegen,
			PositiveInteger hullRegen) throws IllegalArgumentException {

		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new DefenciveSubsystemImpl(name, impactReduction, shieldRegen, hullRegen, capacitorUsage, pgConsumption);
	}

	public DefenciveSubsystemImpl(String name, PositiveInteger impactReduction, PositiveInteger shieldRegen,
			PositiveInteger hullRegen, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.impactReduction = impactReduction;
		this.shieldRegen = shieldRegen;
		this.hullRegen = hullRegen;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		PositiveInteger damage = getDamageAfterReduction(incomingDamage.damage);

		return new AttackAction(damage, incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	private PositiveInteger getDamageAfterReduction(PositiveInteger incomingDamage) {
		double percentOfDamageDealt = this.impactReduction.value() > 95 ? 0.05
				: 1 - this.impactReduction.value() / 100.0;
		double damage = Math.ceil(percentOfDamageDealt * incomingDamage.value());

		return PositiveInteger.of((int) damage);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public String getName() {
		return this.name;
	}

	public PositiveInteger getImpactReduction() {
		return this.impactReduction;
	}

	public PositiveInteger getShieldRegen() {
		return this.shieldRegen;
	}

	public PositiveInteger getHullRegen() {
		return this.hullRegen;
	}

}
