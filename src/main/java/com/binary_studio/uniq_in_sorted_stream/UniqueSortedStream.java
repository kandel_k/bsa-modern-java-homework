package com.binary_studio.uniq_in_sorted_stream;

import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	/*
	 * In this task firstly I used distinct method and override equals, but then found out
	 * that I can create my own Predicate and use it in the filter.
	 */
	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {

		if (stream == null) {
			return Stream.empty();
		}

		return stream.filter(Row.distinctByKey(Row::getPrimaryId));
	}

}
