package com.binary_studio.tree_max_depth;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	// Well, I used a BFS algorithm without using a recursion
	public static Integer calculateMaxDepth(Department rootDepartment) {
		List<Department> nodes;
		int depth = 1;

		if (rootDepartment == null) {
			return 0;
		}

		nodes = rootDepartment.subDepartments.parallelStream().filter(Objects::nonNull).collect(Collectors.toList());

		while (!nodes.isEmpty()) {
			depth++;

			nodes = nodes.parallelStream().flatMap(dep -> dep.subDepartments.stream().filter(Objects::nonNull))
					.collect(Collectors.toList());
		}

		return depth;
	}

}
